const http = require("http");
const fetch = require("node-fetch");
const fs = require("fs");

const port = process.env.FORSIDE_PORT || doThrow("Missing $FORSIDE_PORT");
const clientId =
  process.env.OPENPLATFORM_CLIENTID ||
  doThrow("Missing $OPENPLATFORM_CLIENTID");
const clientSecret =
  process.env.OPENPLATFORM_CLIENTSECRET ||
  doThrow("Missing $OPENPLATFORM_CLIENTSECRET");
const mycrudServer = process.env.MYCRUD_HOST || doThrow("Missing $MYCRUD_HOST");
const apiDomain = process.env.FORSIDE_HOST || doThrow("Missing $FORSIDE_HOST");

const testAgency = "790900";

let openplatformToken = "";

let openplatformCovers, coverDate;
function doThrow(e) {
  throw e;
}
function currentDate() {
  return new Date().toISOString().slice(0, 10);
}
async function getOpenPlatformToken() {
  console.time(`retrieve openplatform-token`);
  let result = await fetch("https://auth.dbc.dk/oauth/token", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization:
        "Basic " +
        Buffer.from(clientId + ":" + clientSecret).toString("base64"),
    },
    body: "grant_type=password&username=@&password=@",
  });
  result = await result.json();
  openplatformToken = result.access_token;
  console.timeEnd(`retrieve openplatform-token`);
}
async function getOpenPlatformCover(pid) {
  if (coverDate !== currentDate()) {
    openplatformCovers = {};
    coverDate = currentDate();
  }
  if (!openplatformCovers.hasOwnProperty(pid)) {
    openplatformCovers[pid] = new Promise(async (resolve) => {
      try {
        let result = await fetch(
          `https://openplatform.dbc.dk/v3/work?access_token=${openplatformToken}`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ pids: [pid], fields: ["coverUrlFull"] }),
          }
        );
        result = await result.json();
        result = result.data[0];
        result = result.coverUrlFull && {
          url: result.coverUrlFull[0],
          source: "openplatform",
        };
        resolve(result);
      } catch (e) {
        reject(e);
      }
    });
  }
  return await openplatformCovers[pid];
}
async function getForsideAppCover({ agency, pid }) {
  try {
    img = await fetch(
      `https://${mycrudServer}/forsider.${agency}.cover/` + pid
    );
    console.log("app.forsider.dk cover", img.status);
    if (img.status === 200) {
      img = Buffer.from(await img.arrayBuffer());
      return {
        data: img,
        source: "app.forsider.dk",
        url: `https://${apiDomain}/${agency}/cover/` + pid,
      };
    }
  } catch (e) {
    console.log(e);
  }
}
async function getCover({ agency, pid }) {
  return (
    (await getOpenPlatformCover(pid)) ||
    (await getForsideAppCover({ agency, pid })) || { source: "nowhere" }
  );
}
async function handleCover({ url, res, agency }) {
  url = url.replace("/cover/", "");
  let pid = decodeURIComponent(url).toLowerCase();
  let cover = await getCover({ agency, pid });
  console.log(agency, "/cover/", pid, cover.source);
  if (cover.data) {
    res.setHeader("Content-Type", "image/jpeg");
    return res.end(cover.data);
  }
  if (cover.url) {
    res.writeHead(302, { Location: cover.url });
    return res.end();
  }

  // TODO default cover
  res.end();
}
async function handleCompatGet({ url, res, agency }) {
  url = url.replace("/compat/", "");
  let domain = url.match("^[a-zA-Z.]*")[0];
  let domainRegex = new RegExp(domain.replace(/\./g, "\\."), "g");
  let result = await fetch(`https://${url}`);
  result = await result.text();
  result = result.replace(
    domainRegex,
    `${apiDomain}/${agency}/compat/${domain}`
  );
  res.setHeader("Content-Type", "text/xml; charset=ISO-8859-1");
  return res.end(result);
}
function processCompatResponse(response, agency) {
  return response.replace(
    /<ns1:identifierInformation>.*?<.*?<.ns1:identifierInformation>/g,
    (s) => {
      if (s.indexOf("<ns1:identifierKnown>true</ns1:identifierKnown>") !== -1)
        return s;
      let pid = s.replace(/.*<ns1:pid>([^<]*).*/, (_, pid) => pid);
      let image = `https://${apiDomain}/${agency}/cover/${pid}`;
      let result = `<ns1:identifierInformation>
       <ns1:identifierKnown>true</ns1:identifierKnown>
       <ns1:identifier><ns1:pid>${pid}</ns1:pid></ns1:identifier>
       <ns1:coverImage imageSize="detail" imageFormat="jpeg">${image}</ns1:coverImage>
       <ns1:coverImage imageSize="detail_117" imageFormat="jpeg">${image}</ns1:coverImage>
       <ns1:coverImage imageSize="detail_256" imageFormat="jpeg">${image}</ns1:coverImage>
       <ns1:coverImage imageSize="detail_500" imageFormat="jpeg">${image}</ns1:coverImage>
     </ns1:identifierInformation>`;
      return result.replace(/\n */g, "");
    }
  );
}
async function handleCompatPost({ url, body, res, agency }) {
  url = url.replace("/compat/", "https://");
  let moreinfoUrl = url;
  let result = await fetch(moreinfoUrl, {
    method: "POST",
    headers: {
      "content-type": "text/xml; charset=utf-8",
      soapaction: '"http://cover.dandigbib.org"',
    },
    body: body.replace(/api.forsider.dk\/[0-9]*\/?compat\//g, ""),
  });
  res.setHeader("Content-Type", "text/xml; charset=ISO-8859-1");
  return res.end(processCompatResponse(await result.text(), agency));
}
async function handleRequest({ method, url, headers, body, req, res }) {
  console.log(method, url);
  let agency = testAgency;
  if (url.match(/^\/[0-9][0-9]*\//)) {
    url = url.slice(1);
    let pos = url.indexOf("/");
    agency = url.slice(0, pos);
    url = url.slice(pos);
  }

  if (url.startsWith("/cover/")) return handleCover({ url, res, agency });
  if (method === "POST" && url.startsWith("/compat/"))
    return handleCompatPost({ url, body, res, agency });
  if (url.startsWith("/compat/")) return handleCompatGet({ url, res, agency });

  res.writeHead(302, { Location: "https://forsider.dk" });
  res.end();
}
async function startServer() {
  console.time(`start listening on port ${port}`);
  const server = http.createServer((req, res) => {
    let body = "";
    req.on("data", (buf) => {
      if (body.length < 16 * 1024 * 1024) {
        body += buf.toString("latin1");
      } else {
        console.error("request body overflow");
      }
    });
    req.on("end", async (o) => {
      let { method, url, headers } = req;
      await handleRequest({ method, url, headers, body, req, res });
    });
  });
  server.listen(port);
  console.timeEnd(`start listening on port ${port}`);
}
async function main() {
  await getOpenPlatformToken();
  setInterval(getOpenPlatformToken, 60 * 60 * 1000);
  await startServer();
}
main();
