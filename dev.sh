(sleep 1; touch server.js) &
  while inotifywait -e modify,close_write,move_self -q *.js
  do 
    kill `cat .pid`
    sleep 0.5
    kill -9 `cat .pid`
    echo ===========================================================
    node server.js $@ &
    echo $! > .pid
    sleep 0.5;
  done
